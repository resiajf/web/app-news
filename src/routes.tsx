import asyncComponent from 'src/common/components/async-component';
import { Route, route } from 'src/common/lib/utils';

export const moduleName = 'news';

const Hello = asyncComponent(() => import('./components/home-page'));
const Adeu = asyncComponent(() => import('./components/stay-connected-page'));

export const routes: Array<Route<any, any>> = [
    route('/', 'home', Hello, 'home', { exact: true }),
    route('/stay_connected', 'stay_connected', Adeu, 'stay_connected', { exact: true }),
];