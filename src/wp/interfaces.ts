interface ContentRendered {
    rendered: string;
    protected?: boolean;
}

export interface Post {
    id: number;
    date: string;
    date_gmt: string;
    content: ContentRendered;
    excerpt: ContentRendered;
    link: string;
    modified: string;
    modified_gmt: string;
    title: ContentRendered;
}