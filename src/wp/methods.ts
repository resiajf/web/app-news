import { Post } from 'src/wp/interfaces';

export class Wordpress {

    public static async getPosts() {
        const res = await fetch(`${Wordpress.BASE}/wp/v2/posts&per_page=8`);
        const json = await res.json();
        return json as Post[];
    }

    private static BASE = 'https://resiajf.ml/noticias/?rest_route=';

}