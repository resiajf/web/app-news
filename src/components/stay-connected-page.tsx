import React from 'react';
import { Trans, WithNamespaces, withNamespaces } from 'react-i18next';

import img from 'src/components/chrome_msg.png';

const PushNotificationsIcon = () => (
    <div style={{ display: 'inline-block', width: 23, height: 23, position: 'static' }}>
    <svg className="onesignal-bell-svg" xmlns="http://www.w3.org/2000/svg" width="99.7" height="99.7"
         viewBox="0 0 99.7 99.7"
         style={{ filter: 'drop-shadow(0 2px 4px rgba(34,36,38,0.35))', width: '100%', height: '100%' }}>
        <circle className="background" cx="49.9" cy="49.9" r="49.9" style={{ fill: '#fff' }} />
        <path className="foreground"
              d="M50.1 66.2H27.7s-2-.2-2-2.1c0-1.9 1.7-2 1.7-2s6.7-3.2 6.7-5.5S33 52.7 33 43.3s6-16.6 13.2-16.6c0 0 1-2.4 3.9-2.4 2.8 0 3.8 2.4 3.8 2.4 7.2 0 13.2 7.2 13.2 16.6s-1 11-1 13.3c0 2.3 6.7 5.5 6.7 5.5s1.7.1 1.7 2c0 1.8-2.1 2.1-2.1 2.1H50.1zm-7.2 2.3h14.5s-1 6.3-7.2 6.3-7.3-6.3-7.3-6.3z"
              style={{ fill: '#e54b4d' }} />
        <ellipse className="stroke" cx="49.9" cy="49.9" rx="37.4" ry="36.9" style={{ fill: 'none', stroke: '#e54b4d', strokeWidth: 3, strokeMiterlimit: 10 }} />
    </svg>
    </div>
);

export default withNamespaces()(({ t }: WithNamespaces) => (
    <div className="lead">
        <h1 className="display-4">{ t('stay_connected_title') }</h1>
        <p>
            { t('stay_connected_text.0') }
        </p>
        <ul>
            <li className="mb-2">
                <b>{ t('stay_connected_text.1') }:</b>&nbsp;{ t('stay_connected_text.2') }
            </li>
            <li className="mb-2">
                <b>Telegram:</b>&nbsp;
                <Trans i18nKey="stay_connected_text.3">
                    . <a href="https://telegram.me/noti_resi_ajf" target="_blank" rel="nofollow">.</a> .
                </Trans>
            </li>
            <li className="mb-2">
                <b>{ t('stay_connected_text.4') }:</b>&nbsp;
                <Trans i18nKey="stay_connected_text.5">
                    . <i>.</i> . <PushNotificationsIcon /> .
                </Trans>
                <br/>
                <div className="text-center">
                    <img src={ img } style={{ maxWidth: 334, width: '100%', height: 'auto' }} />
                </div>
            </li>
        </ul>
    </div>
));