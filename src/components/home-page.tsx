import React from 'react';
import { withNamespaces } from 'react-i18next';
import { Link as Lincc } from 'react-router-dom';

import { Link } from 'src/common/components/bootstrap/buttons';
import LoadSpinner from 'src/common/components/load-spinner';
import { PostEntry } from 'src/components/post-entry';
import { Post } from 'src/wp/interfaces';
import { Wordpress } from 'src/wp/methods';

interface HomePageState {
    error: any | null;
    loading: boolean;
    posts: Post[] | null;
}

class HomePageClass extends React.Component<any, HomePageState> {

    constructor(props: any) {
        super(props);

        this.state = {
            error: null,
            loading: true,
            posts: null,
        };
    }

    public componentDidMount() {
        this.loadPosts();
    }

    public render() {
        const { t } = this.props;

        return (
            <>
                <h1 className="display-4">{ t('resumen') }</h1>
                <div className="text-center mt-1 mb-4">
                    <Link type="primary" size="lg" href="/noticias/">{ t('go_to_wp') }</Link>
                    <br />
                    <Lincc to="/stay_connected" className="btn btn-secondary btn-sm mt-2">{ t('keep_connected') }</Lincc>
                </div>

                <h3 className="text-center">{ t('latest_news') }</h3>
                <div className="d-flex flex-wrap justify-content-center">
                    { this.state.posts ?
                        this.state.posts.map(post => <PostEntry key={ post.id } post={ post } t={ t } />) :
                        <LoadSpinner />
                    }
                </div>
            </>
        );
    }

    private async loadPosts() {
        try {
            this.setState({ loading: true });
            this.setState({
                loading: false,
                posts: await Wordpress.getPosts(),
            });
        } catch(e) {
            this.setState({
                error: e,
                loading: false,
            });
        }
    }

}

export default withNamespaces()(HomePageClass);