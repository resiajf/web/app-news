import moment from 'moment';
import React from 'react';
import { Trans } from 'react-i18next';

import { Post } from 'src/wp/interfaces';

import 'src/styles/ddg-card-style.css';

const openPost = ({ link }: Post) => () => window.open(link);
const dateStr = (dateGmt: string, t: string) => {
    const date = moment.utc(dateGmt);
    if(moment.utc().diff(date) < 7 * 24 * 60 * 60 * 1000) {
        return <Trans i18nKey={ t }>
            . {{ at: date.fromNow() }}
        </Trans>;
    } else {
        return <Trans i18nKey={ `${t}_at` }>
            . {{ when: date.format('L LT') }}
        </Trans>;
    }
};

export const PostEntry = ({ post, t }: any) => (
    <div className="card" onClick={ openPost(post) }>
        <div className="card-body">
            <h5 className="card-title" dangerouslySetInnerHTML={{ __html: post.title.rendered }} />
            <p className="card-text">
                <small className="text-muted">
                    { dateStr(post.date_gmt, 'post_created') }
                    { post.date_gmt !== post.modified_gmt && <>
                    <br/>
                    { dateStr(post.modified_gmt, 'post_modified') }
                    </> }
                </small>
            </p>
            <p className="card-text" dangerouslySetInnerHTML={{ __html: post.excerpt.rendered }} />
        </div>
    </div>
);